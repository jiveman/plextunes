import sys
import os
import ipdb
import libpytunes


from loguru import logger

logger.remove()
logger.add(
    sys.stderr,
    colorize=True,
    format="[<cyan>{time:MM-DD HH:mm:ss}</cyan>] -  <level>{message}</level>",
    level="DEBUG",
)
logger.add(
    "app.log",
    backtrace=True,
    format="{time:MM-DD HH:mm:ss}[{level}] {message}",
    rotation="5 MB",
)


class Track():
    ''' abstract Track class to give working with Plex Tracks or iTunes Tracks similar '''
    # @cache.memoize(expire=86400, tag='utrack')
    def __init__(self, track):
        self._track = track
        if isinstance(track, libpytunes.Song):
            self._kind = 'itunes'
        else:
            self._kind = 'plex'
        self._name = False
        self._play_count = 0
        self._rating = 0.0
        self._artist = False
        self._album = False
        self._bitrate = 0
        self._duration = 0
        self.warm_obj()

    def warm_obj(self):
        for key in ('kind', 'name', 'play_count', 'rating', 'album', 'duration', 'artist'):
            if not getattr(self, key):
                # logger.debug('no {} found for track {}'.format(key, self._track))
                pass


    @property
    def kind(self):
        return self._kind

    @property
    def track(self):
        return self.name


    @property
    def name(self):
        if not self._name:
            if self.kind == 'itunes':
                self._name = self._track.name.strip()
                # if not self._name:
                #     logger.debug('no itunes track name found')
                #     ipdb.set_trace()
            else:
                if self._track.originalTitle:
                    self._name = self._track.originalTitle
                else:
                    self._name = self._track.title.strip()
                # if not self._name:
                #     logger.debug('no plex track name found')
                #     ipdb.set_trace()
        return self._name
    
    @property
    def play_count(self):
        if not self._play_count:
            if self.kind == 'itunes':
                self._play_count = getattr(self._track, 'play_count', 0)
            else:
                self._play_count = self._track.viewCount
                # if not self._play_count:
                #     logger.debug('no plex track play_count found')
                #     ipdb.set_trace()
        return self._play_count

    @property
    def rating(self):
        if not self._rating:
            if self.kind == 'itunes':
                irating = getattr(self._track, 'rating', 0)
                # convert to plex format rating
                self._rating = irating
                if irating:
                    self._rating = repr(irating / 10)
            else:
                self._rating = self._track.userRating
                # if not self._rating:
                #     logger.debug('no plex track rating found')
                #     ipdb.set_trace()
        return self._rating

    @property
    def bitrate(self):
        if not self._bitrate:
            if self.kind == 'itunes':
                self._bitrate = getattr(self._track, 'bit_rate', 0)
            else:
                if self._track.media:
                    self._bitrate = self._track.media[0].bitrate
        return self._bitrate

    @property
    def duration(self):
        if not self._duration:
            if self.kind == 'itunes':
                self._duration = getattr(self._track, 'length', 0)
            else:
                self._duration = self._track.duration
        return self._duration



    @property
    def artist(self):
        if not self._artist:
            if self.kind == 'itunes':
                self._artist = self._track.artist
                if not self._artist:
                    self._artist = self._track.album_artist
                # if not self._artist:
                #     logger.debug('no itunes track artist found')
                #     ipdb.set_trace()
            else:
                self._artist = self._track.artist().title
                # if not self._artist:
                #     logger.debug('no plex track artist found')
                #     ipdb.set_trace()
        return self._artist

    @property
    def album(self):
        if not self._album:
            if self.kind == 'itunes':
                self._album = self._track.album
                # if not self._album:
                #     logger.debug('no itunes track album found')
                #     ipdb.set_trace()
            else:
                self._album = self._track.album().title
                # if not self._album:
                #     logger.debug('no plex track album found')
                #     ipdb.set_trace()
        return self._album

