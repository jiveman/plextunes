# plextunes

Created by Jaime Carranza (jaime@carranza.co)

Plextunes reads in an iTunes library database and creates selected playlists. It iterates over songs in the playlists and attempts to fuzzy match them (doesn't always work if the media/ID3 TAGs etc.. were changed/updated by plex/itunes). It will add all matches and update their metadata (ratings only at this time).

## Usage:

```
# if using virtualenv_wrapper
mkvirtualenv -p `which python3` plextunes
workon plextunes

pip -r requirements.txt
# next run update_plex.py which will guide you through creating a configuration file 'config.ini' and start the process
chmod +x ./update_plex.py # ensures execute bit is turned on
./update_plex.py
# this should present you a menu of playlists read from your iTunes DB
# the process may take several hours for larger libraries

```



