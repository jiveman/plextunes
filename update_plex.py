#!/usr/bin/env python3

import os
import sys
from time import sleep
import configparser

from libpytunes import *
import requests
from plexapi.myplex import MyPlexAccount
from plexapi.server import PlexServer
from fuzzywuzzy import fuzz
from loguru import logger
from bullet import Check, keyhandler, styles
from bullet.charDef import NEWLINE_KEY
from diskcache import Cache
import click


from abstracts import Track

# import ipdb


if not os.path.exists('./logs'):
    os.mkdirs('./logs')
logger.remove()
logger.add(
    sys.stderr,
    colorize=True,
    format="[<cyan>{time:MM-DD HH:mm:ss}</cyan>] -  <level>{message}</level>",
    level="DEBUG",
)
logger.add(
    "logs/app.log",
    backtrace=True,
    format="{time:MM-DD HH:mm:ss}[{level}] {message}",
    rotation="5 MB",
)


cache = Cache(disk_min_file_size=10, directory='.diskcache')

CONFIG_FILE = os.path.join('.', 'config.ini')
itunes_lib = ''
plex_music = ''


config = configparser.ConfigParser()
if (os.path.exists(CONFIG_FILE)):
    logger.info('loading conf {}'.format(CONFIG_FILE))
    config.read(CONFIG_FILE)
    if 'plex' not in config.sections():
        config['plex'] = {}
        config['itunes'] = {}
else:
    logger.info('Creating new config.ini file {}'.format(CONFIG_FILE))
    with open(CONFIG_FILE, 'w') as configfile:
        config['plex'] = {}
        config['itunes'] = {}
        config.write(configfile)


def delete_all_playlists():
    [x.delete() for x in plex.playlists()]
    cache.delete('playlists')

def get_plex_api_session(plex_user, plex_pass, plex_host):
    plex = None
    baseurl = cache.get('plex_baseurl')
    token = cache.get('plex_token')
    if not baseurl or not token:
        logger.debug("Logging {} into Plex".format(plex_user))
        account = MyPlexAccount(plex_user, plex_pass)
        logger.info("Connecting to Plex server [{}]...".format(plex_host))
        plex = account.resource(plex_host).connect()
        cache.set('plex_baseurl', plex._baseurl, expire=86400)
        cache.set('plex_token', plex._token, expire=86400)
    else:
        logger.info("Using previous Plex token session")
        plex = PlexServer(baseurl, token)
    return plex



# @cache.memoize(expire=86400, tag='utrack')
def get_track(track):
    try:
        obj = Track(track)
        return obj
    except Exception as err:
        logger.error(err)
        # ipdb.set_trace()
        raise


# it = itunes_lib.getPlaylist(playlists[3]).tracks[30]
# itt = get_track(it)
# pt =  plex_music.searchTracks(title='Hannah Hunt')[0]
# ptt = get_track(pt)
# ipdb.set_trace()


def fuzzy_match(needle, haystack):
    match = None
    match_highest_score = 0
    logger.debug('looking for "{}" - by "{}"'.format(needle.name, needle.artist))
    for song in haystack:
        logger.debug("fuzzy checking {}".format(song.name))
        trkScore = 1
        albScore = 1
        artScore = 1
        lenScore = 1
        brScore = 1
        pt = song
        # if isinstance(song, abstracts.Track):
        #     pt = song
        # else:
        #     pt = get_track(song)
        if pt.name and needle.name:
            trkScore = fuzz.ratio(needle.name,pt.name)
        if pt.album and needle.album:
            albScore = fuzz.ratio(needle.album,pt.album)
        if pt.artist and needle.artist:
            artScore = fuzz.ratio(needle.artist,pt.artist)
        if pt.duration and needle.duration:
            lenScore = needle.duration - (needle.duration - pt.duration)
        if pt.bitrate and needle.bitrate:
            brScore = needle.bitrate - (needle.bitrate - pt.bitrate)

        # Compute overall match score (product of ratios)
        mScore = trkScore * albScore * artScore + lenScore + brScore
        if match_highest_score < mScore:
            logger.debug("{} {} is the best fuzzy match".format(mScore, song.name))
            match = song
            match_highest_score = mScore
        else:
            logger.debug("{} {} did not fuzzy match".format(mScore, song.name))

    return match


# def add_track_to_plex_playlist(track, playlist):
#     logger.debug("Adding '{}' to playlist [{}]".format(track.name, playlist))
#     plex_playlist = get_or_create_plex_playlist(playlist, track)
    # plex.createPlaylist(plex_playlist, track)

def get_playlist_by_name(name):
    cached = cache.get('playlists')
    if cached and name in cached:
        return cached[name]
    playlists = {x.title: x for x in plex.playlists() }
    if playlists and name in playlists:
        logger.debug('Caching playlists {}'.format(', '.join(playlists.keys())))
        cache.set('playlists', playlists, expire=86400 * 365)
        return playlists[name]
    return ''

def add_track_to_plex_playlist(track, name):
    plexobj = track._track
    playlist = get_playlist_by_name(name)
    if not playlist:
        logger.info("Creating playlist '{}'".format(name))
        # ipdb.set_trace()
        # default_song = plex_music.searchTracks(title="default")[0]
        plex.createPlaylist(name, plexobj)
        logger.debug("[{1}] Added {0}".format(track.name, name))
        return True
        # playlist = [x for x in plex.playlists() if name in x.title]

    if track._track not in playlist:
        logger.debug("[{1}] Adding {0}".format(track.name, name))
        playlist.addItems(plexobj)
    else:
        logger.warning('Track already in playlist, skipping')
    return True
    # return playlist[0]

@cache.memoize(expire=86400, tag='plex_artists')
def plex_get_track_artists(track):
    if not track.artist or track.artist == 'VA' or len(track.artist) <= 3:
        logger.debug("Skipping artist: {}".format(track.artist))
        return []
    return plex_music.searchArtists(title=track.artist)


@cache.memoize(expire=86400, tag='plex_tracks')
def plex_get_tracks(track):
    return plex_music.searchTracks(title=track.name)

@cache.memoize(expire=86400, tag='plex_search')
def fetch_plex_possible_tracks(track):
    pile = set()
    try:
        logger.info("Checking Artist: {}".format(track.artist))
        artists = plex_get_track_artists(track)
        if artists:
            for artist in artists:
                # logger.info("Adding Artist: {} tracks...".format(artist))
                for artist_track in artist.tracks():
                    atrack = get_track(artist_track)
                    try:
                        # logger.debug("adding artist {} track {}".format(atrack.artist, atrack.name))
                        pile.add(atrack)
                    except:
                        logger.err('failed to add {} to pile, err: {}'.format(atrack.name, err))
                # artist_tracks = [x.title.lower() for x in artist.tracks()]
                # logger.info("searching artist tracks: {}".format(artist_tracks))
                # if track.name.lower() in artist_tracks:
                #     logger.info("*match found")
                #     return [artist.track(title=track.name)]
        # ipdb.set_trace()
        # logger.info("looking for {}[{}] > [{}] ".format(track.name, track.rating, track.name ) )
        plex_tracks = plex_get_tracks(track)
        for plex_tracks in plex_tracks:
            ptrack = get_track(plex_tracks)
            try:
                logger.debug("adding {}".format(ptrack.name))
                pile.add(ptrack)
            except:
                logger.err('failed to add {} to pile, err: {}'.format(ptrack.name, err))
    #     if tracks and len(tracks) > 1:
    #         matched_tracks = [x for x in tracks if track.artist.lower() in x.artist().title.lower() ]
    #         matched_tracks = [x for x in matched_tracks if track.name.lower() == u"{}".format(x.title.lower()) ]
    #         if matched_tracks:
    #             return matched_tracks
    #     return tracks
    except Exception as err:
        logger.error(err)
        raise
    #     return None
    return pile

def update_plex_meta(match, itunes):
    plex_track = match._track
    if itunes.rating:
        logger.debug("Updating {} rating ({})".format(match.name, itunes.rating))
        plex_track.edit(**{'userRating.value': itunes.rating})
    # ipdb.set_trace()
    # if itunes.play_count:
    #     logger.debug("Updating {} play count ({})".format(match.name, itunes.play_count))
    #     # payload['viewCount.value'] = itunes.play_count
    #     # plex_track.edit(**{'playCount.value': itunes.play_count})
    #     plex_track.edit(**{'viewCount.value': itunes.play_count})
    # if itunes.



def upsert_playlists(playlists):
    playlist_checkpoint = cache.get('playlist_checkpoint')
    track_checkpoint = cache.get('track_checkpoint')
    for playlist in playlists:
        # playlist = pl[1]
        # playlist_id = pl[0]
        if playlist_checkpoint:
            logger.debug("Resuming Populating Playlist {}".format(playlist_checkpoint))
            if playlist == playlist_checkpoint:
                playlist_checkpoint = False
            else:
                continue
        cache.set('playlist_checkpoint', playlist)
        logger.debug('Fetching {} iTunes tracks'.format(playlist))
        songs = itunes_lib.getPlaylist(playlist).tracks
        # if plex_playlist:
        #     logger.debug("We have fetched plex playlist {}".format(playlist))
        #     continue
        songs.sort(key=lambda x: 0 if x.rating is None else x.rating, reverse=True)

        for song in songs:
            if track_checkpoint:
                logger.debug("Resuming After Track {}".format(track_checkpoint))
                if song.name == track_checkpoint:
                    track_checkpoint = False
                    logger.info("Resume started")
                    continue
                else:
                    continue
            track = get_track(song)
            logger.info("Searching to match [{} ({})]".format(track.name, track.artist))
            haystack = fetch_plex_possible_tracks(track)
            match = fuzzy_match(track, haystack)
            if not match:
                continue
            update_plex_meta(match, track)
            logger.info("Added {} to playlist: {}".format(match.name, playlist))
            # plex.createPlaylist(playlist, match._track)
            add_track_to_plex_playlist(match, playlist)
            cache.set('track_checkpoint', song.name)
        # song = itunes_lib.getPlaylist(playlist).tracks[0]
        # if song.rating:
        #     try:
        #         tracks = fetch_plex_possible_tracks(song)
        #         if tracks:
        #             track = tracks[0]
        #             logger.info(
        #                 " +Adding '{}' to playlist: [{}]".format(track.title, playlist)
        #             )
        #             logger.info("CREATING playlist [{}]".format(playlist))
                    
        #         else:
        #             logger.info("^no match for {}".format(song.name))
        #     except UnicodeEncodeError as err:
        #         logger.info("skipping {} error: {}".format((song.name, err)))

class MinMaxCheck(Check):
    def __init__(self, min_selections=0, max_selections=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.min_selections = min_selections
        self.max_selections = max_selections
        if max_selections is None:
            self.max_selections = len(self.choices)

    @keyhandler.register(NEWLINE_KEY)
    def accept(self):
        if self.valid():
            return super().accept()

    def valid(self):
        return self.min_selections <= sum(1 for c in self.checked if c) <= self.max_selections


def write_config(config_file, conf):
    try:
        with open(config_file, 'w') as out:
          conf.write(out)
    except Exception as err:
        logger.info("failed writing {}: {}".format(config_file, err))
        return False
    logger.opt(ansi = True).info('<green>wrote {}</green>'.format(config_file))
    return True

plex_password = config['plex'].get('password', '')
masked_password = ''
if plex_password:
    masked_password = '{}*******'.format(plex_password[0:2])

@click.command()
@click.option('--username', prompt='Plex Username', help='Plex username', default=config['plex'].get('username', ''))
@click.option('--password', prompt='Plex Password', hide_input=True, confirmation_prompt=True, help='Plex username password', default=masked_password)
@click.option('--host', prompt='Plex Server Name', help='Plex Host or Friedly Server Name as you see it when launching plex.tv', default=config['plex'].get('host', ''))
@click.option('--library_path', type=click.File('rb'), prompt='iTunes Library Path', help='Disk path to iTunes XML database', default=config['itunes'].get('library_path', '') )
def main(username, password, host, library_path):
    if password == masked_password:
        password = plex_password
    config['plex']['username'] = username
    config['plex']['password'] = password
    config['plex']['host'] = host
    config['itunes']['library_path'] = library_path.name
    if not write_config(CONFIG_FILE, config):
        logger.warning('Failed writing [{}], check disk file permissions'.format(CONFIG_FILE))
    logger.debug("loading iTunes DB ({})".format(library_path))
    global itunes_lib
    itunes_lib = Library(library_path)

    logger.debug("loading iTunes playlists")
    playlists = itunes_lib.getPlaylistNames()
    logger.info("Found {} iTunes playlists ({},...)".format(len(playlists), ','.join(playlists[0:10])))

    # playlist_name_and_index = [ (playlists.index(x),x ) for x in playlists]
    # for song in itunes_lib.getPlaylist(playlists[0]).tracks:
    #     if not os.path.exists(song.location):
    #         print song.name
    # song = itunes_lib.getPlaylist(playlists[20]).tracks[1021]


    plex = get_plex_api_session(username, password, host)
    logger.debug("switching into Plex Music")
    global plex_music
    plex_music = plex.library.section("Music")

    # ipdb.set_trace()

    cached_selected_playlists = cache.get('selected_playlists')
    selected_playlists = []
    if cached_selected_playlists:
        logger.info("{},...".format(', '.join(cached_selected_playlists[0:5])))
        ans = input("Do you want to use previously selected ({}) playlists?[y/n] ".format(len(cached_selected_playlists)))
        if ans.lower() == 'y' or ans.lower() == 'yes':
            selected_playlists = cached_selected_playlists
    if not selected_playlists:
        print('\n')
        logger.info("Choose playlists to import by hitting [spacebar] and then hit enter.")
        if not cached_selected_playlists:
            sleep(3)
        client = MinMaxCheck(
            prompt = "\nChoose playlists to import: ",
            min_selections = 1,
            max_selections = 10,
            choices= playlists,
            indent = 0,
            align = 5, 
            margin = 2,
            check= "🎸",
            # check = "🎵",
            # pad_right = 5,
            # **styles.Example,
            # **styles.Exam
        )
        print('\n', end = '')
        selected_playlists = client.launch()
        print(selected_playlists)
        cache.delete('playlist_checkpoint')
        cache.delete('track_checkpoint')
        logger.debug('caching selected playlists for next time')
        cache.set('selected_playlists', selected_playlists, expire=86400 * 7)
    logger.debug('processing playlists: {}'.format(', '.join(selected_playlists)))
    # sys.exit(2)
    upsert_playlists(selected_playlists)

if __name__ == "__main__":
    main()

